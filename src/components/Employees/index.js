import { Col, Row, Table } from 'antd';
import axios from 'axios';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import AddEmployeeButton from './AddEmployeeButton';
import DeleteEmployeeButton from './DeleteEmployeeButton';

const Employees = () => {
	const [allEmployees, setAllEmployees] = useState([]);

	const requestGetEmloyeesList = () => {
		return new Promise((resolve, reject) => {
			axios.get('https://reqres.in/api/users?per_page=12').then(({ data }) => {
				resolve(data);
			});
		});
	};

	const onDeleteEmployee = (id) => {
		setAllEmployees((prev) => prev.filter((employee) => employee.id !== id));
	};

	const onAddEmployee = (data) => {
		setAllEmployees((prev) => [...prev, data]);
	};

	const columns = [
		{
			title: 'Полное имя',
			dataIndex: 'first_name',
			render: (cell, row) => `${cell} ${_.get(row, 'last_name', '')}`,
		},
		{
			dataIndex: 'id',
			width: 100,
			render: (cell) => (
				<DeleteEmployeeButton id={cell} onDelete={onDeleteEmployee} />
			),
		},
	];

	const intFunc = () => {
		requestGetEmloyeesList().then(({ data }) => setAllEmployees(data));
	};

	useEffect(() => {
		intFunc();
	}, []);

	return (
		<Row gutter={[0, 16]}>
			<Col span={24}>
				<AddEmployeeButton onAddEmployee={onAddEmployee} />
			</Col>
			<Col span={24}>
				<Table columns={columns} dataSource={allEmployees} rowKey={'id'} />
			</Col>
		</Row>
	);
};

export default Employees;
