import { Button } from 'antd';

const DeleteEmployeeButton = ({ id, onDelete }) => {
	return (
		<Button danger onClick={() => onDelete(id)}>
			Удалить
		</Button>
	);
};

export default DeleteEmployeeButton;
