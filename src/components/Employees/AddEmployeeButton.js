import { Button, Form, Input, Modal } from 'antd';
import { useState } from 'react';

const AddEmployeeButton = ({ onAddEmployee }) => {
	const [modalVisible, setModalVisible] = useState(false);
	const [newEmployeeData, setNewEmployeeData] = useState(null);

	const toggleModalVisible = () => setModalVisible((prev) => !prev);

	const onOk = () => {
		onAddEmployee({
			...newEmployeeData,
			id: new Date(),
		});
		toggleModalVisible();
	};

	const onChangeData = (e) => {
		const { name, value } = e.target;

		setNewEmployeeData((prev) => ({
			...prev,
			[name]: value,
		}));
	};

	return (
		<>
			<Button type="primary" onClick={toggleModalVisible}>
				Добавить сотрудника
			</Button>
			<Modal
				title="Добавить сотрудника"
				visible={modalVisible}
				onOk={onOk}
				onCancel={toggleModalVisible}
				okText="Подтвердить"
				cancelText="Отмена"
			>
				<Form layout="vertical">
					<Form.Item label="Имя">
						<Input name="first_name" onChange={onChangeData} />
					</Form.Item>
					<Form.Item label="Фамилия" style={{ marginBottom: 0 }}>
						<Input name="last_name" onChange={onChangeData} />
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
};

export default AddEmployeeButton;
