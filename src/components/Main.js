import { Result } from 'antd';

const Main = () => {
	return (
		<Result
			status="success"
			title="Great task"
			subTitle="Hello, it is main page application"
		/>
	);
};

export default Main;
