import { Layout, Menu } from 'antd';
import { Link, Route, Routes } from 'react-router-dom';
import Employees from './components/Employees';
import Main from './components/Main';

function App() {
	return (
		<Layout>
			<Layout.Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
				<Menu theme="dark" mode="horizontal">
					<Menu.Item key="1">
						<Link to="/">Главная</Link>
					</Menu.Item>
					<Menu.Item key="2">
						<Link to="/employees">Сотрудники</Link>
					</Menu.Item>
				</Menu>
			</Layout.Header>
			<Layout.Content
				className="site-layout"
				style={{ padding: '0 50px', marginTop: 64 }}
			>
				<div
					className="site-layout-background"
					style={{ padding: 24, minHeight: 'calc(100vh - 64px)' }}
				>
					<Routes>
						<Route path="/" exact element={<Main />} />
						<Route path="/employees" element={<Employees />} />
					</Routes>
				</div>
			</Layout.Content>
		</Layout>
	);
}

export default App;
